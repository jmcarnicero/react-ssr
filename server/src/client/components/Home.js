import React from "react";

const Home = () => {
  return (
    <div>
      <div>Home component</div>
      <button onClick={() => console.log("click")}>Press here</button>
    </div>
  );
};

export default Home;
