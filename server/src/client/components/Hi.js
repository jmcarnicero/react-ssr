import React from "react";

const Hi = () => {
  return (
    <div>
      <div>Hi component</div>
      <button onClick={() => console.log("click")}>Press here</button>
    </div>
  );
};

export default Hi;
