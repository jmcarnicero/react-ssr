import React from "react";
import { Route } from "react-router-dom";
import Home from "./components/Home";
import Hi from "./components/Hi";

export default () => {
  return (
    <div>
      <Route exact path="/" component={Home} />
      <Route exact path="/hi" component={Hi} />
    </div>
  );
};
